"use strict";

// Теоретичні питання

// 1 - let div = document.createElement('div');
// div.innerHTML = "Hello";
// document.body.appendChild(div);

// 2 - Перший параметр функції указує куди саме потрібно вставити елемент відносно елементу викликаючого метода
// 'beforebegin': вставити HTML перед елементом
// 'afterbegin': на початок елементу
// 'beforeend': в кінець елементу
// 'afterend': після елементу
// 3 - elem.remove("")

// Завдання практичне


function result(arr, parent=document.body) {
    const ul = document.createElement("ul");
    arr.forEach(el => {
        const li = document.createElement("li");
        li.textContent = el
        ul.append(li);
    })
    parent.append(ul);
}

result( ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);



